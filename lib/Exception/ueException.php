<?php
namespace USAePay\Exception;

use \Exception;

class ueException extends \Exception {

    protected $result;
    protected $response;

    public function __construct($message = null, $code = 0, $result = null, $response = null, \Exception $previous = null) {

        $this->result = $result;
        $this->response = $response;

        parent::__construct($message, $code, $previous);
    }

    final public function getResult() {
        return $this->result;
    }

    final public function getResponse() {
        return $this->response;
    }
    
}